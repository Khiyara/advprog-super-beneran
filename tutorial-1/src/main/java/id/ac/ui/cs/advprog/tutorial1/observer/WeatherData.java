package id.ac.ui.cs.advprog.tutorial1.observer;	

import java.util.Observable;

public class WeatherData extends Observable {

    private float temperature;
    private float humidity;
    private float pressure;

    public void measurementsChanged() {
        setChanged();
        notifyObservers();
    }

    public void setMeasurements(float temperature, float humidity,
                                float pressure) {
        this.temperature = temperature;
        this.humidity = humidity;
        this.pressure = pressure;
        measurementsChanged();
    }

    public float getTemperature() {
        if(this.temperature > 0f){
			return this.temperature;
		}
        return 0f;
    }

    public void setTemperature(float temperature) {
        this.temperature = temperature;
		measurementsChanged();
    }

    public float getHumidity() {
        if(this.humidity > 0f){
			return this.humidity;
		}
        return 0f;
    }

    public void setHumidity(float humidity) {
        this.humidity = humidity;
		measurementsChanged();
    }

    public float getPressure() {
        if(this.pressure > 0f){
			return this.pressure;
		}
        return 0f;
    }

    public void setPressure(float pressure) {
        this.pressure = pressure;
		measurementsChanged();
    }
}
