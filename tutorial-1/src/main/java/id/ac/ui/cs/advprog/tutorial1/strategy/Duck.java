package id.ac.ui.cs.advprog.tutorial1.strategy;

public abstract class Duck {

    private FlyBehavior flyBehavior;
    private QuackBehavior quackBehavior;

    public void performFly() {
        flyBehavior.fly();
    }

    public void performQuack() {
        quackBehavior.quack();
    }
	
	public void setFlyBehavior(FlyBehavior fly) {
        flyBehavior = fly;
    }
	
    public void setQuackBehavior(QuackBehavior quak) {
        quackBehavior = quak;
    }
	
	public abstract void swim();
	public abstract void display();
}
