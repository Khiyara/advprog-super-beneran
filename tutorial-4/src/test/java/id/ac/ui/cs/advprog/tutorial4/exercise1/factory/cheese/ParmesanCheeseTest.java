package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ParmesanCheese;
import org.junit.Test;

public class ParmesanCheeseTest {
    private ParmesanCheese parmesanCheese;

    @Test
    public void toStringParmesanCheese() {

        parmesanCheese = new ParmesanCheese();
        assertEquals("Shredded Parmesan",parmesanCheese.toString());

    }
}
