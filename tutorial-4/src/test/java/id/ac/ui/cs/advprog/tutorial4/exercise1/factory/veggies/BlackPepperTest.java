package id.ac.ui.cs.advprog.tutorial4.exercise1.veggies;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.BlackPepper;
import org.junit.Test;

public class BlackPepperTest {
    private BlackPepper blackPepper;

    @Test
    public void toStringGreenPepper() {

        blackPepper = new BlackPepper();
        assertEquals("Black Pepper",blackPepper.toString());

    }
}
