package id.ac.ui.cs.advprog.tutorial4.exercise1.veggies;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Garlic;
import org.junit.Test;

public class GarlicTest {
    private Garlic garlic;

    @Test
    public void toStringGarlic() {

        garlic = new Garlic();
        assertEquals("Garlic",garlic.toString());

    }
}
