package id.ac.ui.cs.advprog.tutorial4.exercise1.clam;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.CookedClams;
import org.junit.Test;

public class CookedClamsTest {
    private CookedClams cookedClam;

    @Test
    public void toStringCookedClams() {

        cookedClam = new CookedClams();
        assertEquals("Cooked Clams",cookedClam.toString());

    }
}
