package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ItalianoCheese;
import org.junit.Test;

public class ItalianoCheeseTest {
    private ItalianoCheese italianoCheese;

    @Test
    public void toStringItalianoCheese() {

        italianoCheese = new ItalianoCheese();
        assertEquals("Italiano Cheese",italianoCheese.toString());

    }
}
