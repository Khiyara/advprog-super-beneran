package id.ac.ui.cs.advprog.tutorial4.exercise1.veggies;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.RedPepper;
import org.junit.Test;

public class RedPepperTest {
    private RedPepper redPepper;

    @Test
    public void toStringRedPepper() {

        redPepper = new RedPepper();
        assertEquals("Red Pepper",redPepper.toString());

    }
}
