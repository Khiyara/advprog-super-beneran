package id.ac.ui.cs.advprog.tutorial4.exercise1.veggies;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Onion;
import org.junit.Test;

public class OnionTest {
    private Onion onion;

    @Test
    public void toStringOnion() {

        onion = new Onion();
        assertEquals("Onion",onion.toString());

    }
}
