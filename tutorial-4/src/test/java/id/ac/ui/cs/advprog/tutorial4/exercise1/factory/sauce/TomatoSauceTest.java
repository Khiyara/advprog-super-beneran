package id.ac.ui.cs.advprog.tutorial4.exercise1.sauce;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.TomatoSauce;
import org.junit.Test;

public class TomatoSauceTest {
    private TomatoSauce tomatoSauce;

    @Test
    public void toStringTomatoSauce() {

        tomatoSauce = new TomatoSauce();
        assertEquals("Tomato Sauce",tomatoSauce.toString());

    }
}
