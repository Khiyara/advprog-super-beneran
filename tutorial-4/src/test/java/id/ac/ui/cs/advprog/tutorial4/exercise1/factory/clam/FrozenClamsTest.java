package id.ac.ui.cs.advprog.tutorial4.exercise1.clam;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FrozenClams;
import org.junit.Test;

public class FrozenClamsTest {
    private FrozenClams frozenClams;

    @Test
    public void toStringFrozenClams() {

        frozenClams = new FrozenClams();
        assertEquals("Frozen Clams from Chesapeake Bay",frozenClams.toString());

    }
}
