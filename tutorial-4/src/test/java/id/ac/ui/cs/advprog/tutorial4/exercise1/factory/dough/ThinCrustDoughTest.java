package id.ac.ui.cs.advprog.tutorial4.exercise1.dough;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThinCrustDough;
import org.junit.Test;

public class ThinCrustDoughTest {
    private ThinCrustDough thinCrustDough;

    @Test
    public void toStringThinCrustDough() {

        thinCrustDough = new ThinCrustDough();
        assertEquals("Thin Crust Dough",thinCrustDough.toString());

    }
}
