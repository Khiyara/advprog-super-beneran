package id.ac.ui.cs.advprog.tutorial4.exercise1.dough;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThickCrustDough;
import org.junit.Test;

public class ThickCrustDoughTest {
    private ThickCrustDough thickCrustDough;

    @Test
    public void toStringThickCrustDough() {

        thickCrustDough = new ThickCrustDough();
        assertEquals("ThickCrust style extra thick crust dough",thickCrustDough.toString());

    }
}
