package id.ac.ui.cs.advprog.tutorial4.exercise1.dough;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.WithoutCrustDough;
import org.junit.Test;

public class WithoutCrustDoughTest {
    private WithoutCrustDough noCrustDough;

    @Test
    public void toStringWithoutCrustDough() {

        noCrustDough = new WithoutCrustDough();
        assertEquals("No Crust Dough",noCrustDough.toString());

    }
}
