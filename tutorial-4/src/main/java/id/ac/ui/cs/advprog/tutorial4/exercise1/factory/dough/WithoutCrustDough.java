package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class WithoutCrustDough implements Dough {
    public String toString() {
        return "No Crust Dough";
    }
}
