package hello;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = CVController.class)
public class CreateCVTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void cvWithoutUser() throws Exception {
        mockMvc.perform(get("/mycv"))
                .andExpect(content().string(containsString("This is my CV")));
    }

    @Test
    public void cvWithUser() throws Exception {
        mockMvc.perform(get("/mycv").param("visitor", "Pepega"))
                .andExpect(content().
                    string(containsString("Pepega I hope you are interested to hire me")));
    }
}
